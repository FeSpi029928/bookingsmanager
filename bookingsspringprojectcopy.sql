-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Set 16, 2020 alle 18:22
-- Versione del server: 10.4.8-MariaDB
-- Versione PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bookingsspringprojectcopy`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `agency`
--

CREATE TABLE `agency` (
  `id` int(11) NOT NULL,
  `administrator_user_id` int(11) DEFAULT NULL,
  `agency_code` varchar(45) DEFAULT NULL,
  `counter` int(11) NOT NULL,
  `creation_time` datetime DEFAULT NULL,
  `creation_user` varchar(255) DEFAULT NULL,
  `headquarters_number` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `users_groups_number` int(11) DEFAULT NULL,
  `administrator_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `booking_code` varchar(45) DEFAULT NULL,
  `booking_date` datetime DEFAULT NULL,
  `booking_type` varchar(45) DEFAULT NULL,
  `category` varchar(45) DEFAULT NULL,
  `creation_time` datetime DEFAULT NULL,
  `creation_user` varchar(255) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `headquarter`
--

CREATE TABLE `headquarter` (
  `id` int(11) NOT NULL,
  `administrator_user_id` int(11) DEFAULT NULL,
  `creation_time` datetime DEFAULT NULL,
  `creation_user` varchar(255) DEFAULT NULL,
  `headquarter_code` varchar(45) DEFAULT NULL,
  `spaces_number` int(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `headquarter_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `regards`
--

CREATE TABLE `regards` (
  `id` int(11) NOT NULL,
  `booking_id` int(11) DEFAULT NULL,
  `creation_time` datetime DEFAULT NULL,
  `creation_user` varchar(255) DEFAULT NULL,
  `workstation_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `space`
--

CREATE TABLE `space` (
  `id` int(11) NOT NULL,
  `administrator_user_id` int(11) DEFAULT NULL,
  `creation_time` datetime DEFAULT NULL,
  `creation_user` varchar(255) DEFAULT NULL,
  `space_code` varchar(45) DEFAULT NULL,
  `workstations_number` int(11) DEFAULT NULL,
  `headquarter_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `administered_agencies_number` int(11) DEFAULT NULL,
  `administered_headquarters_number` int(11) DEFAULT NULL,
  `creation_time` datetime DEFAULT NULL,
  `creation_user` varchar(255) DEFAULT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `fiscal_code` varchar(16) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `password` varchar(65) DEFAULT NULL,
  `residential_address` varchar(45) DEFAULT NULL,
  `roles` varchar(45) DEFAULT NULL,
  `user_code` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `user_group_id` int(11) DEFAULT NULL,
  `bookings_number` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `user_group`
--

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL,
  `creation_time` datetime DEFAULT NULL,
  `creation_user` varchar(255) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `user_group_code` varchar(45) DEFAULT NULL,
  `users` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `workstation`
--

CREATE TABLE `workstation` (
  `id` int(11) NOT NULL,
  `creation_time` datetime DEFAULT NULL,
  `creation_user` varchar(255) DEFAULT NULL,
  `workstation_code` varchar(45) DEFAULT NULL,
  `space_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `agency`
--
ALTER TABLE `agency`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKefinoxhyjq9mu6o6jupb8pr1c` (`administrator_user_id`);

--
-- Indici per le tabelle `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKkgseyy7t56x7lkjgu3wah5s3t` (`user_id`);

--
-- Indici per le tabelle `headquarter`
--
ALTER TABLE `headquarter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKje0cbmj8kfy6w63u89wbgd15a` (`agency_id`),
  ADD KEY `FK3fie5jkn8doag9354th5x0j5m` (`administrator_user_id`);

--
-- Indici per le tabelle `regards`
--
ALTER TABLE `regards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKcjdnilt3shn8k5qu1v60ow2tp` (`booking_id`),
  ADD KEY `FKks80e5cnvm1nb34tyvolnhkk4` (`workstation_id`);

--
-- Indici per le tabelle `space`
--
ALTER TABLE `space`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKcntnl2tra5e8729a38ao0woj2` (`headquarter_id`);

--
-- Indici per le tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK3x269n17oftkff7pg3uxj0niy` (`agency_id`),
  ADD KEY `FKd5uhmsqhax1l70pck9lmgphjr` (`user_group_id`);

--
-- Indici per le tabelle `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `workstation`
--
ALTER TABLE `workstation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKe1bdxkp08g2sq7uy6xr98nejb` (`space_id`),
  ADD KEY `FK4ii6ixhlphilej6ejnkilygh` (`user_id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `agency`
--
ALTER TABLE `agency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `headquarter`
--
ALTER TABLE `headquarter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `regards`
--
ALTER TABLE `regards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `space`
--
ALTER TABLE `space`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `workstation`
--
ALTER TABLE `workstation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `agency`
--
ALTER TABLE `agency`
  ADD CONSTRAINT `FKefinoxhyjq9mu6o6jupb8pr1c` FOREIGN KEY (`administrator_user_id`) REFERENCES `user` (`id`);

--
-- Limiti per la tabella `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `FKkgseyy7t56x7lkjgu3wah5s3t` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Limiti per la tabella `headquarter`
--
ALTER TABLE `headquarter`
  ADD CONSTRAINT `FK3fie5jkn8doag9354th5x0j5m` FOREIGN KEY (`administrator_user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FKje0cbmj8kfy6w63u89wbgd15a` FOREIGN KEY (`agency_id`) REFERENCES `agency` (`id`);

--
-- Limiti per la tabella `regards`
--
ALTER TABLE `regards`
  ADD CONSTRAINT `FKcjdnilt3shn8k5qu1v60ow2tp` FOREIGN KEY (`booking_id`) REFERENCES `booking` (`id`),
  ADD CONSTRAINT `FKks80e5cnvm1nb34tyvolnhkk4` FOREIGN KEY (`workstation_id`) REFERENCES `workstation` (`id`);

--
-- Limiti per la tabella `space`
--
ALTER TABLE `space`
  ADD CONSTRAINT `FKcntnl2tra5e8729a38ao0woj2` FOREIGN KEY (`headquarter_id`) REFERENCES `headquarter` (`id`);

--
-- Limiti per la tabella `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK3x269n17oftkff7pg3uxj0niy` FOREIGN KEY (`agency_id`) REFERENCES `agency` (`id`),
  ADD CONSTRAINT `FKd5uhmsqhax1l70pck9lmgphjr` FOREIGN KEY (`user_group_id`) REFERENCES `user_group` (`id`);

--
-- Limiti per la tabella `workstation`
--
ALTER TABLE `workstation`
  ADD CONSTRAINT `FK4ii6ixhlphilej6ejnkilygh` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FKe1bdxkp08g2sq7uy6xr98nejb` FOREIGN KEY (`space_id`) REFERENCES `space` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
